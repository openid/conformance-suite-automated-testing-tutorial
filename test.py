#!/usr/bin/env python3

import asyncio
import json
import os

from conformance import Conformance
from dotenv import load_dotenv

load_dotenv()

CONFORMANCE_SERVER = "https://www.certification.openid.net/"
CONFORMANCE_TOKEN = os.environ["CONFORMANCE_TOKEN"]
# CONFORMANCE_TOKEN is created using the token management
# page at https://www.certification.openid.net/tokens.html
# and, in this example, placed in a .env file, e.g:
# CONFORMANCE_TOKEN=b3MuZW52aXJvblsiQ09ORk9STUFOQ0VfVE9LRU4iXQ==

# This is the name of the Basic OP test plan:
test_plan_name = "oidcc-basic-certification-test-plan"

# This is the variant configuration of the test, 
# i.e. static or dynamic metadata location and client registration:
test_variant_config = { 
    "server_metadata": "discovery",
    "client_registration": "static_client" 
}

# This is the required configuration for the test run:
test_plan_config = {
    "alias": "conformance-tutorial-com",
    "description": "OIDCC Basic OP conformance testing",
    "server": {
        "discoveryUrl": "https://conformance.sandbox.signicat.com/auth/open/.well-known/openid-configuration"
    },
    "client": {
        "client_id": "sandbox-elated-parrot-234",
        "client_secret": os.environ["CLIENT1_SECRET"]
    },
    "client_secret_post": {
        "client_id": "sandbox-elated-parrot-234",
        "client_secret": os.environ["CLIENT_SECRET_POST"]
    },
    "client2": {
        "client_id": "sandbox-zany-rabbit-303",
        "client_secret": os.environ["CLIENT2_SECRET"]
    },
    "browser": [
        {
            "match": "https://conformance.sandbox.signicat.com/auth/open/connect/authorize*",
            "tasks": [
                {
                    "task": "Choose login method",
                    "optional": True,
                    "match": "https://conformance.sandbox.signicat.com/broker/authn/simulator/selection",
                    "commands": [
                        [ "click", "css", "a[data-test='selection-idin']" ]
                    ]
                },
                {
                    "task": "Complete login",
                    "optional": True,
                    "match": "https://conformance.sandbox.signicat.com/broker/authn/simulator/authenticate/*",
                    "commands": [
                        [ "text", "id", "dateOfBirth", "1975-07-25", "optional"],
                        [ "click", "css", "button.mdc-button" ]
                    ]
                },
                {
                    "task": "Verify complete",
                    "match": "*/test/*/callback*",
                    "commands": [
                        [ "wait", "id", "submission_complete", 10 ]
                    ]
                }
            ]
        },
    ],
    "override":  {

        "oidcc-response-type-missing": {
            "browser": [
                {
                    "match": "https://conformance.sandbox.signicat.com/auth/open/connect/authorize*",
                    "tasks": [
                        {
                            "task": "Verify error",
                            "match": "https://conformance.sandbox.signicat.com/auth/open/Error*",
                            "commands": [
                                    [ "wait", "xpath", "//*", 10, "Error", "update-image-placeholder" ]
                            ]
                        }
                    ]
                },
            ]
        },

        "oidcc-prompt-login": {
            "browser": [
                {
                    "match": "https://conformance.sandbox.signicat.com/auth/open/connect/authorize*",
                    "tasks": [
                        {
                            "task": "Choose login method",
                            "match": "https://conformance.sandbox.signicat.com/broker/authn/simulator/selection",
                            "commands": [
                                [ "wait", "xpath", "//*", 10, "Select simulator", "update-image-placeholder-optional" ],
                                [ "click", "css", "a[data-test='selection-idin']" ]
                            ]
                        },
                        {
                            "task": "Complete login",
                            "match": "https://conformance.sandbox.signicat.com/broker/authn/simulator/authenticate/*",
                            "commands": [
                                [ "click", "css", "button.mdc-button" ]
                            ]
                        },
                        {
                            "task": "Verify complete",
                            "match": "*/test/*/callback*",
                            "commands": [
                                [ "wait", "id", "submission_complete", 10 ]
                            ]
                        }
                    ]
                }
            ]
        },

        "oidcc-max-age-1": {
            "browser": [
                {
                    "match": "https://conformance.sandbox.signicat.com/auth/open/connect/authorize*",
                    "tasks": [
                        {
                            "task": "Choose login method",
                            "match": "https://conformance.sandbox.signicat.com/broker/authn/simulator/selection",
                            "commands": [
                                [ "wait", "xpath", "//*", 10, "Select simulator", "update-image-placeholder-optional" ],
                                [ "click", "css", "a[data-test='selection-idin']" ]
                            ]
                        },
                        {
                            "task": "Complete login",
                            "match": "https://conformance.sandbox.signicat.com/broker/authn/simulator/authenticate/*",
                            "commands": [
                                [ "click", "css", "button.mdc-button" ]
                            ]
                        },
                        {
                            "task": "Verify complete",
                            "match": "*/test/*/callback*",
                            "commands": [
                                [ "wait", "id", "submission_complete", 10 ]
                            ]
                        }
                    ]
                }
            ]
        },

        "oidcc-ensure-registered-redirect-uri": {
            "browser": [
                {
                    "match": "https://conformance.sandbox.signicat.com/auth/open/connect/authorize*",
                    "tasks": [
                        {
                            "task": "Verify error",
                            "match": "https://conformance.sandbox.signicat.com/auth/open/Error*",
                            "commands": [
                                    [ "wait", "xpath", "//*", 10, "Error", "update-image-placeholder" ]
                            ]
                        }
                    ]
                },
            ]
        },

        "oidcc-refresh-token": {
            "browser": [
                {
                    "match": "https://conformance.sandbox.signicat.com/auth/open/connect/authorize*",
                    "tasks": [
                        {
                            "task": "Choose login method",
                            "optional": True,
                            "match": "https://conformance.sandbox.signicat.com/broker/authn/simulator/selection",
                            "commands": [
                                [ "click", "css", "a[data-test='selection-idin']" ]
                            ]
                        },
                        {
                            "task": "Complete login",
                            "optional": True,
                            "match": "https://conformance.sandbox.signicat.com/broker/authn/simulator/authenticate/*",
                            "commands": [
                                [ "text", "id", "dateOfBirth", "1975-07-25", "optional"],
                                [ "click", "css", "button.mdc-button" ]
                            ]
                        },
                        {
                            "task": "Give consent",
                            "optional": True,
                            "match": "https://conformance.sandbox.signicat.com/auth/open/consent?*",
                            "commands": [
                                [ "click", "css", "button.mdc-button:nth-child(1)" ]
                            ]
                        },
                        {
                            "task": "Verify complete",
                            "match": "*/test/*/callback*",
                            "commands": [
                                [ "wait", "id", "submission_complete", 10 ]
                            ]
                        }
                    ]
                },
            ]
        }

    }
}


# Create a Conformance instance...
conformance = Conformance(CONFORMANCE_SERVER, CONFORMANCE_TOKEN, verify_ssl=True)

# Create a test plan instance and print the id of it
test_plan = asyncio.run(conformance.create_test_plan(test_plan_name, json.dumps(test_plan_config), test_variant_config))
plan_id = test_plan['id']

print("----------------\nBegin {}.".format(test_plan_name))
print('Plan URL: {}plan-detail.html?plan={}\n'.format(CONFORMANCE_SERVER, plan_id))

# Iterate over the tests in the plan and run them one by one
for test in test_plan["modules"]:

    # Fetch name and variant of the next test to run
    module_name = test["testModule"]
    variant = test["variant"]
    print("Module name: {}".format(module_name))
    print("Variant: {}".format(json.dumps(variant)))

    # Create an instance of that test
    module_instance = asyncio.run(conformance.create_test_from_plan_with_variant(plan_id, module_name, variant))
    module_id = module_instance['id']
    print('Test URL: {}log-detail.html?log={}'.format(CONFORMANCE_SERVER, module_id))

    # Run the test and wait for it to finish
    state = asyncio.run(conformance.wait_for_state(module_id, ["FINISHED"]))
    print("")

print('Plan URL: {}plan-detail.html?plan={}\n'.format(CONFORMANCE_SERVER, plan_id))
print("\nEnd {}\n----------------".format(test_plan_name))

print("Creating certification package")
asyncio.run(conformance.create_certification_package(
    plan_id = plan_id,
    conformance_pdf_path = "OpenID-Certification-of-Conformance.pdf", 
    output_zip_directory = "./zips/")
)
