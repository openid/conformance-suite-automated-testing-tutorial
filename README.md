In this tutorial, we'll set up automated testing of an authorization server using the Basic OP conformance profile test plan — in a step-by-step fashion. For that purpose, we'll be using a Python helper library in a script that will eventually:

 - Create a test plan.
 - Configure the test plan.
 - Automate the interactive steps of the test plan.
 - Create a certification package .zip file that can be submitted for certification.

The intention of this walkthrough is to provide an understanding of each step involved in setting up your own automated conformance testing of your authorization server, and it is intentionally written on a detailed and basic level.

# 1. Setting up the Python project

We start by creating a new directory for our project and activating a [Python virtual environment](https://docs.python.org/3/tutorial/venv.html) in it.

```bash
$ mkdir conformance-testing
$ cd conformance-testing
$ python3 -m venv .venv
$ source .venv/bin/activate
```

Copy the [`conformance.py`](https://gitlab.com/openid/conformance-suite/-/blob/master/scripts/conformance.py) file into your directory. Additionally, add a file [`requirements.txt`](requirements.txt) that we will provide to [pip](https://pypi.org/project/pip/) with the following content:

```python
asyncio
httpx
python-dotenv
requests

```

In order to be able to create and administer tests on behalf of your conformance suite user, go to [the token management page](https://www.certification.openid.net/tokens.html) and create a permanent token. In your project, create a file called `.env` and add a `CONFORMANCE_TOKEN` environment variable to it, like so:

```python
# This is an illustration and not an actual token
CONFORMANCE_TOKEN=Q3JlYXRlWW91clRva2VuT25UaGVUb2tlbk1hbmFnZW1lbnRQYWdl
```

Now let's create the Python script that will control the tests, let's call it [`test.py`](test.py). We start by importing the dependencies that we will eventually need and loading up the conformance suite URL and token:


```python
#!/usr/bin/env python3

import asyncio
import json
import os

from conformance import Conformance
from dotenv import load_dotenv

load_dotenv()

CONFORMANCE_SERVER = "https://www.certification.openid.net/"
CONFORMANCE_TOKEN = os.environ["CONFORMANCE_TOKEN"]
# CONFORMANCE_TOKEN is created using the token management
# page at https://www.certification.openid.net/tokens.html
# and, in this example, placed in a .env file, e.g:
# CONFORMANCE_TOKEN=Q3JlYXRlWW91clRva2VuT25UaGVUb2tlbk1hbmFnZW1lbnRQYWdl

print(CONFORMANCE_TOKEN)

```

We're now ready to run our file and make sure that it prints the token and executes without errors. Let's first install the dependencies, then run the script:

```bash
(.venv) $ pip install -r requirements.txt
(.venv) $ python test.py
Q3JlYXRlWW91clRva2VuT25UaGVUb2tlbk1hbmFnZW1lbnRQYWdl

```

By now, we've done all the groundwork and can proceed to the actual test automation.

# 2. Configuring and creating the test plan

Here, we'll be looking at perhaps the most foundational conformance profile of them all — Basic OP. In order to do that, we'll need access to a basic OpenID provider (you will presumably use your own), and we'll need to set up a couple of clients. This part of the setup directly reflects the configuration we do when creating a test plan instance in [the conformance suite UI](https://www.certification.openid.net/schedule-test.html).

Once we've acquired the discoveryUrl, the clients and the credentials, we add it to the end of our [`test.py`](test.py) file, like so:

```python
# This is the name of the Basic OP test plan:
test_plan_name = "oidcc-basic-certification-test-plan"

# This is the variant configuration of the test, 
# i.e. static or dynamic metadata location and client registration:
test_variant_config = { 
    "server_metadata": "discovery",
    "client_registration": "static_client" 
}

# This is the required configuration for the test run:
test_plan_config = {
    "alias": "conformance-tutorial-com",
    "description": "OIDCC Basic OP conformance testing",
    "server": {
        "discoveryUrl": "https://conformance.sandbox.signicat.com/auth/open/.well-known/openid-configuration"
    },
    "client": {
        "client_id": "sandbox-elated-parrot-234",
        "client_secret": "chngtRf...2ECd35I"
    },
    "client_secret_post": {
        "client_id": "sandbox-elated-parrot-234",
        "client_secret": "chngtRf...2ECd35I"
    },
    "client2": {
        "client_id": "sandbox-zany-rabbit-303",
        "client_secret": "RooUfQi...bpTqVf6t"
    }
}
```

Please note that the `alias` property will determine certain URLs in your test, such as the `redirect_uri`. In other words, if the alias is set to `conformance-tutorial-com` as in the example above, then the clients you've configured in your OP need to allow https://www.certification.openid.net/test/a/conformance-tutorial-com/callback as a `redirect_uri`.

Anyway, by now, we're all set up to actually create and run a test, so let's do that. We create an instance of the `Conformance` class (from our supporting `conformance.py` library), and then we use that instance to create a test plan using the test plan configuration demonstrated above:

```python
# Create a Conformance instance...
conformance = Conformance(CONFORMANCE_SERVER, CONFORMANCE_TOKEN, verify_ssl=True)

# Create a test plan instance and print the id of it
test_plan = asyncio.run(conformance.create_test_plan(test_plan_name, json.dumps(test_plan_config), test_variant_config))
plan_id = test_plan['id']
print('Plan URL: {}plan-detail.html?plan={}'.format(CONFORMANCE_SERVER, plan_id))

```

At this point, let's try to execute our script to see that it works as intended, i.e. it outputs the newly created plan id.

```bash
(.venv) $ python test.py
Plan URL: https://www.certification.openid.net/plan-detail.html?plan=GrlqQjMDrcjbk
```

If you visit the plan URL in your browser, you can see that it has been created accordingly, but of course the tests all have status "NOT RUN" at this point.

![](./img/screenshot1.png)

# 2. Executing the first test in the test plan

If you look at the `test_plan` object above, you'll see that it's a `dict` with the following structure:


```python
{
  "name": "oidcc-basic-certification-test-plan",
  "id": "GrlqQjMDrcjbk",
  "modules": [
    {
      "testModule": "oidcc-server",
      "variant": {
        "client_auth_type": "client_secret_basic",
        "response_type": "code",
        "response_mode": "default"
      },
      "instances": []
    },
    {
      "testModule": "oidcc-response-type-missing",
      "variant": {
        "client_auth_type": "client_secret_basic",
        "response_type": "code",
        "response_mode": "default"
      },
      "instances": []
    }, …
```

The entries in the `modules` array each correspond to an individual test in the test plan. Let's modify our script code in order to execute the first test, the `oidcc-server` one which is the general happy-path test for Basic OP:

```python
test = test_plan['modules'][0]   # Get the first test module,
module_name = test["testModule"] # get the name of it
variant = test["variant"]        # and the variant/configuration

# Conformance.create_test_from_plan_with_variant is an async function,
# but in this script we're running the tests synchronously so
# we perform the test execution in an asyncio sync wrapper
module_instance = asyncio.run(conformance.create_test_from_plan_with_variant(plan_id, module_name, variant))
module_id = module_instance['id']
print('Test URL: {}log-detail.html?log={}'.format(CONFORMANCE_SERVER, module_id))

# Run the test and wait for it to finish
state = asyncio.run(conformance.wait_for_state(module_id, ["FINISHED"]))
```

Executing our script now has the following result:

```bash
(.venv) $ python test.py
Plan URL: https://www.certification.openid.net/plan-detail.html?plan=S3QwGYz959J7f

Test URL: https://www.certification.openid.net/log-detail.html?log=2AuTdXTRbYOPMBv
module id 2AuTdXTRbYOPMBv status is CREATED
module id 2AuTdXTRbYOPMBv status is CREATED
module id 2AuTdXTRbYOPMBv status is WAITING
module id 2AuTdXTRbYOPMBv status is WAITING
…
```

OK, so the test is created and started, but it doesn't seem to get out of its WAITING state. Let's visit the "Test URL" to see what's going on:

![](./img/screenshot2.png)

We see that the test is waiting for someone/something to visit the OP authorization endpoint. If we manually click that button, *for the specific OP that we're using in this example*, a UI is displayed which is expecting the end user to select an option. This leads us to the next section of this tutorial.

# 3. Automating the interactive steps of the tests

By extending our test plan configuration, we can instruct the conformance suite to perform certain steps for us [using a built-in Selenium instance](https://gitlab.com/openid/conformance-suite/-/wikis/Design/BrowserControl). We do this by appending a `browser` section to the `test_plan_config`, like so:

```python
test_plan_config = {
    "alias": "conformance-tutorial-com",
    "description": "OIDCC Basic OP conformance testing",
    "server": {
        "discoveryUrl": "https://conformance.sandbox.signicat.com/auth/open/.well-known/openid-configuration"
    },
    …
    "browser": [
        {
            "match": "https://conformance.sandbox.signicat.com/auth/open/connect/authorize*",
            "tasks": [
                {
                    "task": "Choose login method",
                    "optional": True,
                    "match": "https://conformance.sandbox.signicat.com/broker/authn/simulator/selection",
                    "commands": [
                        [ "click", "css", "a[data-test='selection-idin']" ]
                    ]
                }
            ]
        }
    ]
}
```

The [BrowserControl document](https://gitlab.com/openid/conformance-suite/-/wikis/Design/BrowserControl) explains it in more detail, but basically what the newly appended `browser` section does is to say that when the test gets to a point where it will want to launch/redirect to a URL that matches the URL pattern in the `match` value, then a set of tasks should be executed.

In this particular case, the test will eventually want to go to the URL:

```
https://conformance.sandbox.signicat.com/auth/open/connect/authorize?client_id=sandbox-elated-parrot-234&redirect_uri=https://www.certification.openid.net/test/a/conformance-tutorial-com/callback&scope=openid&response_type=code
```

which matches the pattern (notice the wild card at the end):

```
https://conformance.sandbox.signicat.com/auth/open/connect/authorize*
```

To understand what the "Choose login method" task does: If you visit the [`/authorize`](https://conformance.sandbox.signicat.com/auth/open/connect/authorize?client_id=sandbox-elated-parrot-234&redirect_uri=https://www.certification.openid.net/test/a/conformance-tutorial-com/callback&scope=openid&response_type=code) URL then you'll see that you'll instantly be redirected to the URL `https://conformance.sandbox.signicat.com/broker/authn/simulator/selection` which is a page that's expecting user input by choosing one of the options:

![](./img/screenshot3.png)

The `task` that we have defined will `match` on that URL and then perform the specified `commands`. In this case, the Selenium instance will `click` on the element that corresponds to the `css` selector `a[data-test='selection-idin']` (which is the option named "iDIN" in the screenshot).

However, doing so will take us to yet another page that is expecting user input and which has a bunch of input fields and a submit button. For the sake of demonstration, we add another task to our `browser` configuration that has two commands: The first instructs the Selenium instance to locate the DOM element with the `id` attribute equal to `dateOfBirth`, and to set the value of that element to `1975-07-25`. The second command then locates the submit button and clicks it. The corresponding section is inserted right after the previous "Choose login method" task that we had, and it looks like this:

```python
    {
        "task": "Complete login",
        "optional": True,
        "match": "https://conformance.sandbox.signicat.com/broker/authn/simulator/authenticate/*",
        "commands": [
            [ "text", "id", "dateOfBirth", "1975-07-25", "optional"],
            [ "click", "css", "button.mdc-button" ]
        ]
    }
```

Almost done! Let's add a third and final task, following the advice in [the BrowserControl documentation](https://gitlab.com/openid/conformance-suite/-/wikis/Design/BrowserControl), "It's recommended that task lists always include a verification task with no command objects to ensure all redirects and other items return as expected". Hence, we add a third task corresponding to that:

```python
    {
        "task": "Verify complete",
        "match": "*/test/*/callback*",
        "commands": [
            [ "wait", "id", "submission_complete", 10 ]
        ]
    }
```

The command `[ "wait", "id", "submission_complete", 10 ]` simply waits for 10 seconds before it tries to locate a DOM element with `id` `submission_complete`, which the conformance test callback page of course contains.

To recap, the following is now our complete configuration:

```python
test_plan_config = {
    "alias": "conformance-tutorial-com",
    "description": "OIDCC Basic OP conformance testing",
    "server": {
        "discoveryUrl": "https://conformance.sandbox.signicat.com/auth/open/.well-known/openid-configuration"
    },
    "client": {
        "client_id": "sandbox-elated-parrot-234",
        "client_secret": "chngtRf...2ECd35I"
    },
    "client_secret_post": {
        "client_id": "sandbox-elated-parrot-234",
        "client_secret": "chngtRf...2ECd35I"
    },
    "client2": {
        "client_id": "sandbox-zany-rabbit-303",
        "client_secret": "RooUfQi...bpTqVf6t"
    },
    "browser": [
        {
            "match": "https://conformance.sandbox.signicat.com/auth/open/connect/authorize*",
            "tasks": [
                {
                    "task": "Choose login method",
                    "optional": True,
                    "match": "https://conformance.sandbox.signicat.com/broker/authn/simulator/selection",
                    "commands": [
                        [ "click", "css", "a[data-test='selection-idin']" ]
                    ]
                },
                {
                    "task": "Complete login",
                    "optional": True,
                    "match": "https://conformance.sandbox.signicat.com/broker/authn/simulator/authenticate/*",
                    "commands": [
                        [ "text", "id", "dateOfBirth", "1975-07-25", "optional"],
                        [ "click", "css", "button.mdc-button" ]
                    ]
                },
                {
                    "task": "Verify complete",
                    "match": "*/test/*/callback*",
                    "commands": [
                        [ "wait", "id", "submission_complete", 10 ]
                    ]
                }
            ]
        },
    ]
}
```

We're now ready to re-attempt executing our script:

```bash
(.venv) $ python test.py
Plan URL: https://www.certification.openid.net/plan-detail.html?plan=DSn9vuCR5yoHA

Test URL: https://www.certification.openid.net/log-detail.html?log=YWVoYytCFltZIVP
module id YWVoYytCFltZIVP status is CREATED
module id YWVoYytCFltZIVP status is WAITING
module id YWVoYytCFltZIVP status is WAITING
module id YWVoYytCFltZIVP status is RUNNING
module id YWVoYytCFltZIVP status is FINISHED
```

✅ **Success!**

# 4. Getting the rest of the tests to pass

Let's modify our script so that — instead of just running the first test — we run all of the tests in the Basic OP test plan. To accomplish that, we replace our previous logic to run only the first test with the following:

```python
# Iterate over the tests in the plan and run them one by one
for test in test_plan["modules"]:

    # Fetch name and variant of the next test to run
    module_name = test["testModule"]
    variant = test["variant"]
    print("Module name: {}".format(module_name))
    print("Variant: {}".format(json.dumps(variant)))

    # Create an instance of that test
    module_instance = asyncio.run(conformance.create_test_from_plan_with_variant(plan_id, module_name, variant))
    module_id = module_instance['id']
    print('Test URL: {}log-detail.html?log={}'.format(CONFORMANCE_SERVER, module_id))

    # Run the test and wait for it to finish
    state = asyncio.run(conformance.wait_for_state(module_id, ["FINISHED"]))
    print("")
```

Executing the script works well on the first test, but we run into problems already on the next test:

```bash
(.venv) $ python test.py
Plan URL: https://www.certification.openid.net/plan-detail.html?plan=45Wkgl6IhM7sF

Module name: oidcc-server
Variant: {"client_auth_type": "client_secret_basic", "response_type": "code", "response_mode": "default"}
Test URL: https://www.certification.openid.net/log-detail.html?log=oUlrhpx8gkJmHmj
module id oUlrhpx8gkJmHmj status is CREATED
module id oUlrhpx8gkJmHmj status is WAITING
module id oUlrhpx8gkJmHmj status is WAITING
module id oUlrhpx8gkJmHmj status is RUNNING
module id oUlrhpx8gkJmHmj status is FINISHED

Module name: oidcc-response-type-missing
Variant: {"client_auth_type": "client_secret_basic", "response_type": "code", "response_mode": "default"}
Test URL: https://www.certification.openid.net/log-detail.html?log=YQgTnDDxTS9fo7u
module id YQgTnDDxTS9fo7u status is CREATED
module id YQgTnDDxTS9fo7u status is WAITING
module id YQgTnDDxTS9fo7u status is INTERRUPTED

```

What happened? Visiting the test URL provides a hint:

![](./img/screenshot4.png)

Our OP has responded with an error, which isn't that strange given that the test at hand ("oidcc-response-type-missing") has the following description:

> This test sends an authorization request that is missing the response_type parameter. The authorization server must either redirect back with an 'unsupported_response_type' or 'invalid_request' error, or must display an error saying the response type is missing, a screenshot of which should be uploaded.

In order to address negative tests such as this one, we must be able to override the browser automation we defined in the previous section, for certain tests. And to do that, we simply add an `override` section to the configuration, like so:

```python
    "override":  {
        "oidcc-response-type-missing": {
            "browser": [
                {
                    "match": "https://conformance.sandbox.signicat.com/auth/open/connect/authorize*",
                    "tasks": [
                        {
                            "task": "Verify error",
                            "match": "https://conformance.sandbox.signicat.com/auth/open/Error*",
                            "commands": [
                                    [ "wait", "xpath", "//*", 10, "Error", "update-image-placeholder" ]
                            ]
                        }
                    ]
                },
            ]
        }
    }
```

This override means, that for the test called "oidcc-response-type-missing", when the test wants to go the `/authorize` URL (like before), then instead of having the previous three tasks there for user input etc, now we're expecting it to redirect to an error page which will have a URL that matches the `match` value in the task. Once that URL is hit, the Selenium instance should execute the command specified, which can be explained like this: `wait` for `10` seconds and then apply the `xpath` expression `//*` (which selects the whole document) and evaluate the regexp `Error` (which will match if the document contains "Error") and finally apply the action `update-image-placeholder` which puts the HTML source of the page into the test log and marks the test condition as satisfied.

Using our override, we can successfully complete the missing response type test for this OP, **but** we eventually run into similar issues with other tests that are all addressed in a similar manner. For brevity, the details are left out in this tutorial, but we end up with a total of five overrides, all of which can be inspected in the complete [`test.py`](test.py) file.

# 5. Publish the test plan for certification

Now that we've successfully completed the automation of all the tests in the Basic OP test plan, we can optionally publish the results, create a certification package file and submit it for [certification](https://openid.net/certification/) to the OpenID Foundation. In order to do that, we'll need to fill out and sign a Certification of Conformance document. An example of such a document is available [here](OpenID-Certification-of-Conformance.pdf), and the instructions for filling it out are available on [the OpenID Foundation website](https://openid.net/certification/op_submission/).

Publishing the results and obtaining the certification package .zip file for submission can also be done using the [`conformance.py`](https://gitlab.com/openid/conformance-suite/-/blob/master/scripts/conformance.py) library. Assuming that we've filled out and signed a Certification of Conformance document in advance, we can then call the `create_certification_package` function to achieve all of the aforementioned objectives. Hence, at the end of our script we append the following:

```python
print("Creating certification package")
asyncio.run(conformance.create_certification_package(
    plan_id = plan_id,
    conformance_pdf_path = "OpenID-Certification-of-Conformance.pdf", 
    output_zip_directory = "./zips/")
)
```

This function call will download the complete zip file into the specified directory, and then it's simply a matter of [submitting it for certification](https://openid.net/certification/instructions/).

▪️

And with that, we've reached the end of this tutorial. By now, you should be able to continue to build on the concepts and examples here in order to cover your own software with automated conformance testing.

For a list of all available test plans, please see [the conformance suite](https://www.certification.openid.net/schedule-test.html).

